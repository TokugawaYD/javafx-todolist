package Presenter.Components.TaskEditor;

import Presenter.ObservableEntities.ObservableTask;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;


public class TaskEditorController {

  private static TaskEditorController selfSoleInstance;

  @FXML private TextField taskTitleTextField;
  @FXML private TextArea taskNoteTextArea;
  @FXML private Button saveChangesButton;

  private ObservableTask targetTask;

  private BooleanProperty allInputsAreValid;


  public static void beginTaskEditing(ObservableTask targetTask) {

    TaskEditorController selfSoleInstance = TaskEditorController.getSelfSoleInstance();

    selfSoleInstance.targetTask = targetTask;
    selfSoleInstance.taskTitleTextField.setText(targetTask.getTitle().getValue());
    selfSoleInstance.taskNoteTextArea.setText(targetTask.getNote().getValue());

    selfSoleInstance.validatedInputtedData();
  }


  @FXML
  private void initialize() {

    TaskEditorController.selfSoleInstance = this;

    this.allInputsAreValid = new SimpleBooleanProperty(false);
    this.taskTitleTextField.focusedProperty().addListener(
        (observable, hasBeingFocused, isBeingFocusedNow) -> {
          if (!isBeingFocusedNow) {
            allInputsAreValid.setValue(this.isInputtedDataValid());
          }
        }
    );
    this.saveChangesButton.disableProperty().bind(this.allInputsAreValid.not());
  }

  @FXML
  public void saveEditedTask() {

    this.targetTask.setTitle(this.taskTitleTextField.getText());
    this.targetTask.setNote(this.taskNoteTextArea.getText());

    System.out.println(this.targetTask.getTitle().getValue());
    System.out.println(this.targetTask.getNote().getValue());
    System.out.println(this.taskTitleTextField.getText());
    System.out.println(this.taskNoteTextArea.getText());
  }


  private void validatedInputtedData() {
    this.allInputsAreValid.setValue(this.isInputtedDataValid());
  }

  private boolean isInputtedDataValid() {
    return this.taskTitleTextField.getText().length() > 1;
  }

  private static TaskEditorController getSelfSoleInstance() {
    return TaskEditorController.selfSoleInstance;
  }
}
