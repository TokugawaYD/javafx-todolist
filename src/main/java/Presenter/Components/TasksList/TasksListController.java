package Presenter.Components.TasksList;

import BusinessRules.Entities.Task;
import Gateways.Tasks.Retrieving.TasksGateway;

import Presenter.Components.TaskEditor.TaskEditorController;
import Presenter.ObservableEntities.ObservableTask;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.HBox;

import java.io.ByteArrayInputStream;
import java.io.IOException;


public class TasksListController {

  /* --- Dependencies ----------------------------------------------------------------------------------------------- */
  public interface Dependencies {
    TasksGateway tasksGateway();
  }

  private static boolean dependenciesSetupDone = false;

  public static void setDependencies(Dependencies dependencies) {

    if (TasksListController.dependenciesSetupDone) {
      throw new Error("'personOverviewController' already has been initialized.");
    }

    TasksListController.tasksGateway = dependencies.tasksGateway();

    TasksListController.dependenciesSetupDone = true;
  }


  /* --- Data ------------------------------------------------------------------------------------------------------- */
  private static TasksGateway tasksGateway;


  /* --- GUI Elements ----------------------------------------------------------------------------------------------- */
  @FXML private ListView<ObservableTask> tasksListView;


  @FXML
  private void initialize() {

    if (!TasksListController.dependenciesSetupDone) {
      throw new Error("You need to call 'TasksListController.setDependencies()' before use this controller.");
    }

    ObservableList<ObservableTask> observableTasks = FXCollections.observableArrayList();

    for (Task task: TasksListController.tasksGateway.retrieveAll()) {
      observableTasks.add(new ObservableTask(task));
    }

    this.tasksListView.setCellFactory(list -> new TaskCard());
    this.tasksListView.setItems(observableTasks);

    this.tasksListView.getSelectionModel().selectedItemProperty().addListener(
        (observable, oldValue, newValue)-> this.onSelectTask(newValue)
    );
  }


  private void onSelectTask(ObservableTask targetTask) {
    TaskEditorController.beginTaskEditing(targetTask);
  }


  private static class TaskCard extends ListCell<ObservableTask> {


    /* 〔 Theory 〕 Saves the FXML file content to not read from drive each time. */
    private static byte[] cachedComponentTemplate;

    /* 〔 Theory 〕 Must be one per instance because it holds the hierarchy and bindings for each cell (card). */
    FXMLLoader loader = new FXMLLoader();


    private HBox rootElement;
    @FXML private Label titleLabel;
    @FXML private Label noteLabel;
    @FXML private CheckBox isTaskDoneCheckBox;

    public TaskCard() {
      this.initializeTemplate();
    }


    private void initializeTemplate() {

      try {

        if (TaskCard.cachedComponentTemplate == null) {
          TaskCard.cachedComponentTemplate = this.getClass().
              getResource("TaskCard.fxml").
              openStream().
              readAllBytes();
        }

        loader.setController(this);
        this.rootElement = loader.load(new ByteArrayInputStream(TaskCard.cachedComponentTemplate));

      } catch (IOException exception) {
        System.out.println("Failed to load 'TaskCard.fxml' component:\n" + exception.getMessage());
      }
    }


    @Override
    public void updateItem(ObservableTask observableTask, boolean doesNotRepresentAnyDomainData) {

      super.updateItem(observableTask, doesNotRepresentAnyDomainData);

      if (doesNotRepresentAnyDomainData) {
        return;
      }

      this.setGraphic(this.rootElement);

      this.titleLabel.textProperty().bind(observableTask.getTitle());

      if (observableTask.getNote() != null) {
        this.noteLabel.textProperty().bind(observableTask.getNote());
      }

      this.isTaskDoneCheckBox.selectedProperty().bindBidirectional(observableTask.getIsDone());
    }
  }
}
