package Presenter.ObservableEntities;

import BusinessRules.Entities.Task;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;


public class ObservableTask {

  private StringProperty title;
  private StringProperty note;
  private BooleanProperty isDone;


  public ObservableTask(Task task) {

    this.title = new SimpleStringProperty(task.getTitle());
    this.note = new SimpleStringProperty(task.getNote());
    this.isDone = new SimpleBooleanProperty(task.getIsDone());
  }


  public ObservableTask(Task.RequiredParameters requiredParameters) {
    this.title = new SimpleStringProperty(requiredParameters.title());
  }


  public StringProperty getTitle() {
    return this.title;
  }
  public void setTitle(String newTitle) {
    this.title.set(newTitle);
  }

  public StringProperty getNote() {
    return this.note;
  }
  public void setNote(String newNote) {
    this.note.set(newNote);
  }

  public BooleanProperty getIsDone() {
    return this.isDone;
  }
}
