package Gateways.Tasks.Retrieving;


import BusinessRules.Entities.Task;

import java.util.*;


public class MockTasksGateway extends TasksGateway {

  @Override
  public List<Task> retrieveAll() {

    List<Integer> arrangeableTaskIdentifiers = new ArrayList<>();
    List<Task> arrangedTasks = new ArrayList<>();

    for (String ID: this.mockStorage.keySet()) {
      arrangeableTaskIdentifiers.add(Integer.parseInt(ID));
    }

    Collections.sort(arrangeableTaskIdentifiers);

    for (Integer taskID: arrangeableTaskIdentifiers) {

      Task targetTask = this.mockStorage.get(String.valueOf(taskID));

      if (targetTask != null) {
        arrangedTasks.add(targetTask);
      }
    }

    return arrangedTasks;
  }

  @Override
  public Task retrieveByID(String ID) {

    Task desiredTask = this.mockStorage.get(ID);

    if (desiredTask == null) {
      throw new Error(String.format("The task with ID '%s' not found.", ID));
    }

    return desiredTask;
  }

  @Override
  public void add(Task task) {
    this.mockStorage.put(task.getID(), task);
  }


  private final Map<String, Task> mockStorage = Map.ofEntries(

      new AbstractMap.SimpleEntry<>(
          MockTasksGateway.generateID(),
          new Task(new Task.RequiredParameters() {
            public String ID() { return MockTasksGateway.currentID; }
            public String title() { return "Get up"; }
          })
      ),

      new AbstractMap.SimpleEntry<>(
          MockTasksGateway.generateID(),
          new Task(
            new Task.RequiredParameters() {
              public String ID() { return MockTasksGateway.currentID; }
              public String title() { return "Wash the face"; }
            }
          )
              .setNote("With cold water.")
      ),

      new AbstractMap.SimpleEntry<>(
          MockTasksGateway.generateID(),
          new Task(
              new Task.RequiredParameters() {
                public String ID() { return MockTasksGateway.currentID; }
                public String title() { return "Clean the teeth"; }
              }
          )
              .setNote("At least 5 minutes.")
      ),

      new AbstractMap.SimpleEntry<>(
          MockTasksGateway.generateID(),
          new Task(
              new Task.RequiredParameters() {
                public String ID() { return MockTasksGateway.currentID; }
                public String title() { return "Take the breakfast"; }
              }
          )
              .setNote("Toast, egg and beacon.")
      )
  );


  private static int counterForID_Generation = -1;
  private static String currentID = "-1";

  private static String generateID() {
    MockTasksGateway.counterForID_Generation++;
    MockTasksGateway.currentID = String.valueOf(MockTasksGateway.counterForID_Generation);
    return MockTasksGateway.currentID;
  }
}
