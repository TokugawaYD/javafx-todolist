package Gateways.Tasks.Retrieving;

import BusinessRules.Entities.Task;

import java.util.List;


public abstract class TasksGateway {

  public abstract List<Task> retrieveAll();

  public abstract Task retrieveByID(String ID);

  public abstract void add(Task task);
}
