package BusinessRules.Entities;


public class Task {

  private static final int TITLE__MINIMAL_SYMBOLS = 2;
  private static final int TITLE__MAXIMAL_SYMBOLS = 64;

  private static final int NOTE__MINIMAL_SYMBOLS = 2;
  private static final int NOTE__MAXIMAL_SYMBOLS = 127;

  private final String ID;
  private String title;
  private String note;
  private boolean isDone;


  public interface RequiredParameters {
    String ID();
    String title();
  }


  public Task(RequiredParameters requiredParameters) {
    this.ID = requiredParameters.ID();
    this.title = requiredParameters.title();
  }


  public String getID() { return this.ID; }

  public String getTitle() { return title; }
  public Task setTitle(String title) {
    this.title = title;
    return this;
  }

  public String getNote() { return this.note; }
  public Task setNote(String note) {
    this.note = note;
    return this;
  }

  public boolean getIsDone() {
    return this.isDone;
  }
  public void setIsDone(boolean newValue) {
    this.isDone = newValue;
  }
}
